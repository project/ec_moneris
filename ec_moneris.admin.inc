<?php


/**
 * @file
 * Administration settings for Moneris Payment Gateway
 *
 */

/**
 * Moneris Settings Page
 */
function ec_moneris_settings() {
  $form = array();

  $form['ec_moneris_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Use Live or Sandbox Settings'),
    '#default_value' => variable_get('ec_moneris_mode', 0),
    '#options' => array( 1 => t('Live'), 0 => t('Sandbox')),
    '#description' => t('Use Live for normal store operation.'),
  );
  $form['ec_moneris_avs'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Address Verification Service (AVS)'),
    '#default_value' => variable_get('ec_moneris_avs', 0),
    '#description' => t('This will enable address verification.'),
  );
  $form['ec_moneris_cvd'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Card Validation Digits (CVD)'),
    '#default_value' => variable_get('ec_moneris_cvd', 0),
    '#description' => t('This will enable verification of the numbers on the back of the card.'),
  );
  $form['live'] = array(
    '#type' => 'fieldset',
    '#title' => t('Live Settings'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#description' => t('You should set these values for normal operation of your store'),
  );
  $form['live']['ec_moneris_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('ec_moneris_username', ''),
    '#description' => t("Enter your Moneris Username."),
  );
  $form['live']['ec_moneris_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('ec_moneris_password', ''),
    '#description' => t("Enter your Moneris Password."),
  );
  $form['live']['ec_moneris_store_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Store Id'),
    '#default_value' => variable_get('ec_moneris_store_id', ''),
    '#description' => t("Enter your Moneris store id."),
  );
  $form['live']['ec_moneris_api_token'] = array(
    '#type' => 'textfield',
    '#title' => t('API Token'),
    '#default_value' => variable_get('ec_moneris_api_token', ''),
    '#description' => t("Enter your Moneris API Token."),
  );
  $form['sandbox'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sandbox Settings'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#description' => t('You should set these values for testing the operation of your store'),
  );
  $form['sandbox']['ec_moneris_username_sandbox'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('ec_moneris_username_sandbox', 'DemoUser'),
    '#description' => t("Enter your Moneris Username."),
    '#required' => TRUE,
  );
  $form['sandbox']['ec_moneris_password_sandbox'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('ec_moneris_password_sandbox', 'password'),
    '#description' => t("Enter your Moneris Password."),
    '#required' => TRUE,
  );
  $form['sandbox']['ec_moneris_store_id_sandbox'] = array(
    '#type' => 'textfield',
    '#title' => t('Store Id'),
    '#default_value' => variable_get('ec_moneris_store_id_sandbox', 'store1'),
    '#description' => t("Enter your Moneris store id."),
    '#required' => TRUE,
  );
  $form['sandbox']['ec_moneris_api_token_sandbox'] = array(
    '#type' => 'textfield',
    '#title' => t('API Token'),
    '#default_value' => variable_get('ec_moneris_api_token_sandbox', 'yesguy'),
    '#description' => t("Enter your Moneris API Token."),
    '#required' => TRUE,
  );
  $options = array(
    '0' => t('Decline'),
    '1' => t('Approve'),
  );
  $form['avs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Address Verification Settings (AVS)'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#description' => t('Determine action based on AVS response codes. Moneris DOES NOT reject transactions based on AVS or CVD. It passes that information back to allow you, the merchant, to decide what you will accept or decline.'),
  );
  $form['avs']['ec_moneris_avs_response_a'] = array(
    '#type' => 'select',
    '#title' => t('Street Addresses match but postal/Zip codes do not'),
    '#options' => $options,
    '#default_value' => variable_get('ec_moneris_avs_response_a', '0' ),
    '#description' => t('Street addresses match. The street addresses match but the postal/ZIP codes do not, or the request does not include the postal/ZIP code.'),
  );
  $form['avs']['ec_moneris_avs_response_b'] = array(
    '#type' => 'select',
    '#title' => t('Street Addresses match but cannot verify postal/Zip code'),
    '#options' => $options,
    '#default_value' => variable_get('ec_moneris_avs_response_b', '0' ),
    '#description' => t('Street addresses match. The street addresses match but the postal/ZIP codes do not, or the request does not include the postal/ZIP code.'),
  );
  $form['cvd'] = array(
    '#type' => 'fieldset',
    '#title' => t('Card Validation Digits Settings (CVD)'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#description' => t('Determine action based on CVD response codes'),
  );
  $form['debug'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debug Settings'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#description' => t('Data to save for test purposes'),
  );
  $form['debug']['ec_moneris_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Capture data to watchdog for troubleshooting'),
    '#default_value' => variable_get('ec_moneris_debug', ''),
    '#description' => t("Watchdog entries will be created for each of the items you check below."),
  );
  $form['debug']['ec_moneris_debug_request'] = array(
    '#type' => 'checkbox',
    '#title' => t('Capture data sent to Moneris'),
    '#default_value' => variable_get('ec_moneris_debug_request', ''),
    '#description' => t(""),
  );
  $form['debug']['ec_moneris_debug_response'] = array(
    '#type' => 'checkbox',
    '#title' => t('Capture data received from to Moneris'),
    '#default_value' => variable_get('ec_moneris_debug_response', ''),
    '#description' => t(""),
  );
  
  return system_settings_form($form);
}